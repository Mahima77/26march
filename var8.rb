
#It can also be used to make a method return before the last expression is evaluated.
def two_plus_two
  return 2 + 2
  1 + 1  # this expression is never evaluated
end
x = two_plus_two();
puts "#{x}"