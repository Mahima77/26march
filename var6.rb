class C
  def [](a, b)
    puts a + b
  end

  def []=(a, b, c,d)
    puts a * b + c -d
  end
end

obj = C.new

obj[2, 3]     # prints "5"
obj[2, 3 ,4] = 6# prints "10"
